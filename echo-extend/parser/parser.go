package parser

import (
	"github.com/labstack/echo/v4"
)

type Parser[T any] struct {
	name   string
	binder echo.Binder
}

func (b *Parser[T]) Parser(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		payload := new(T)
		err := b.binder.Bind(payload, c)
		if err != nil {
			return c.JSON(400, err)
		}
		c.Set(b.name, payload)
		err = next(c)

		return err
	}
}

func NewDefaultParser[T any](name string) IParser {
	return &Parser[T]{
		name:   name,
		binder: &echo.DefaultBinder{},
	}
}
