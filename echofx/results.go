package echofx

import (
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"
)

type Result struct {
	fx.Out
	EchoApp *echo.Echo `name:"echoApp"`
}
