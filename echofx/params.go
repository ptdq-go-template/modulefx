package echofx

import (
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"
)

type Params struct {
	fx.In
	EchoApp *echo.Echo `name:"echoApp"`
	Port    int        `name:"echoPort"`
}

func main() {
	echo.New()
}
