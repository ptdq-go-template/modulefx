package echofx

import (
	"context"
	"fmt"
	"go.uber.org/fx"
)

const token = "echo-fx"

var Module = fx.Module(token, fx.Invoke(RegisterEchoHooks))

func RegisterEchoHooks(
	lifecycle fx.Lifecycle,
	params Params,
) Result {
	lifecycle.Append(
		fx.Hook{
			OnStart: func(context.Context) error {

				go params.EchoApp.Start(fmt.Sprintf(":%d", params.Port))
				return nil
			},
			OnStop: func(ctx context.Context) error {
				return params.EchoApp.Shutdown(ctx)

			},
		},
	)
	return Result{
		EchoApp: params.EchoApp,
	}

}
